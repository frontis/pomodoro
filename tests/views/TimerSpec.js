import Timer from '../../js/views/Timer.js'

const ENABLED_START_BUTTON = '<button id="start">Start</button>'
const DISABLED_START_BUTTON = '<button id="start" disabled>Start</button>'
const ENABLED_PAUSE_BUTTON = '<button id="pause">Pause</button>'
const DISABLED_PAUSE_BUTTON = '<button id="pause" disabled>Pause</button>'
const DISABLED_RESET_BUTTON = '<button id="reset" disabled>Reset</button>'
const ENABLED_RESET_BUTTON = '<button id="reset">Reset</button>'

describe('Timer', () => {
  describe('when the timer is running', () => {
    let timeRunning = null

    beforeEach(() => {
      timeRunning = { isPaused: false, minutes: 24 }
    })

    it('draws disabled start button', () => {
      const timer = new Timer()

      const template = timer.render(timeRunning)

      expect(template).toContain(DISABLED_START_BUTTON)
    })

    it('draws enabled pause button', () => {
      const timer = new Timer()

      const template = timer.render(timeRunning)

      expect(template).toContain(ENABLED_PAUSE_BUTTON)
    })

    it('draws enabled restart button', () => {
      const timer = new Timer()

      const template = timer.render(timeRunning)

      expect(template).toContain(ENABLED_RESET_BUTTON)
    })
  })

  describe('when the countdown is paused', () => {
    let paused = null

    beforeEach(() => {
      paused = { isPaused: true, minutes: 24 }
    })

    it('draws a enabled start button', () => {
      const timer = new Timer()

      const template = timer.render(paused)

      expect(template).toContain(ENABLED_START_BUTTON)
    })

    it('draws a disabled pause button', () => {
      const timer = new Timer()

      const template = timer.render(paused)

      expect(template).toContain(DISABLED_PAUSE_BUTTON)
    })

    it('draws a enabled reset button', () => {
      const timer = new Timer()

      const template = timer.render(paused)

      expect(template).toContain(ENABLED_RESET_BUTTON)
    })
  })

  describe('when the timer does not have time left', () => {
    let noTimeLeft = null

    beforeEach(() => {
      noTimeLeft = { isPaused: false, minutes: 0 }
    })

    it('draws enabled start button', () => {
      const timer = new Timer()

      const template = timer.render(noTimeLeft)

      expect(template).toContain(ENABLED_START_BUTTON)
    })

    it('draws disabled pause button', () => {
      const timer = new Timer()

      const template = timer.render(noTimeLeft)

      expect(template).toContain(DISABLED_PAUSE_BUTTON)
    })

    it('draws disabled reset button', () => {
      const timer = new Timer()

      const template = timer.render(noTimeLeft)

      expect(template).toContain(DISABLED_RESET_BUTTON)
    })
  })
})
